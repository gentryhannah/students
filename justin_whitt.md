I am Justin Whitt, and I work at the Oak Ridge Leadership Computing Facility at ORNL.  I have worked at ORNL for the past 7 years.
Before that I was an instructor and researcher at the National Center For Computational Engineering at UT Chattanooga.  My 
background is in computational fluid dynamics, but I have a growing interest in data analysis particularly in the synthesis of data.
I have not had the opportunity to write much code in recent years and am excited about rolling up my sleeves again.